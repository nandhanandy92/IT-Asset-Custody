﻿using IT_Asset_Custody.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using IT_Asset_Custody.DAL;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(IT_Asset_Custody.Startup))]
namespace IT_Asset_Custody
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createRolesandUsers();

        }
        // In this method we will create default User roles and Admin user for login   
        private void createRolesandUsers()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            if (!roleManager.RoleExists("Admin"))
            {

                // first we create Admin rool   
                var role = new IdentityRole
                {
                    Name = "Admin"
                };
                roleManager.Create(role);

                //Here we create a Admin super user who will maintain the website   for Ravi.            

                var user = new ApplicationUser
                {
                    UserName = "Ravi.D",
                    Email = "ravi.d@hihindia.org"
                };

                string userPWD = "Sonic2017@";

                var chkUser = UserManager.Create(user, userPWD);

                //Add Ravi to Role Admin   
                if (chkUser.Succeeded)
                {
                    IdentityResult result1 = UserManager.AddToRole(user.Id, "Admin");

                }
                //Here we create a Admin super user who will maintain the website   for Nandha.         

                var user1 = new ApplicationUser();
                user1.UserName = "Nandha";
                user1.Email = "nandhagopal.k@hihseed.org";

                string userPWD1 = "Sonic2017@";

                IdentityResult chkUser1 = UserManager.Create(user1, userPWD1);

                //Add Nandha to Role Admin   
                if (chkUser.Succeeded)
                {
                    IdentityResult result2 = UserManager.AddToRole(user1.Id, "Admin");

                }
            }
            // creating IT-Enginner role 
            if (!roleManager.RoleExists("IT-Engineer"))
            {
                IdentityRole role = new IdentityRole
                {
                    Name = "IT-Engineer"
                };
                roleManager.Create(role);

                //Creating IT-Enginner role for venkat

                ApplicationUser Engineer1 = new ApplicationUser
                {
                    UserName = "Venkat",
                    Email = "venkat.m@hihseed.org"
                };

                string userPWD3 = "Sonic2017@";

                IdentityResult chkUser1 = UserManager.Create(Engineer1, userPWD3);

                //Add IT-Engineer  Role to  Venkat
               
                if (chkUser1.Succeeded)
                {
                    IdentityResult result3 = UserManager.AddToRole(Engineer1.Id, "IT-Engineer");

                }

                //Creating IT-Enginner role for Rajasekar

                ApplicationUser Engineer2 = new ApplicationUser
                {
                    UserName = "Rajasekar",
                    Email = "rajasekar.b@hihseed.org"
                };

                string userPWD4 = "Sonic2017@";

                IdentityResult chkUser2 = UserManager.Create(Engineer2, userPWD4);

                //Add IT-Engineer  Role to  Rajasekar

                if (chkUser2.Succeeded)
                {
                    IdentityResult result4 = UserManager.AddToRole(Engineer2.Id, "IT-Engineer");

                }

                //Creating IT-Enginner role for Radhakrishnan

                ApplicationUser Engineer3 = new ApplicationUser
                {
                    UserName = "Radhakrishnan",
                    Email = "Radhakrishnan.v@hihseed.org"
                };

                string userPWD5 = "Sonic2017@";

                IdentityResult chkUser3 = UserManager.Create(Engineer3, userPWD5);

                //Add IT-Engineer  Role to  Rajasekar

                if (chkUser3.Succeeded)
                {
                    IdentityResult result5 = UserManager.AddToRole(Engineer3.Id, "IT-Engineer");

                }

                //Creating IT-Enginner role for Manikandan

                ApplicationUser Engineer4 = new ApplicationUser
                {
                    UserName = "Radhakrishnan",
                    Email = "Radhakrishnan.v@hihseed.org"
                };

                string userPWD6 = "Sonic2017@";

                IdentityResult chkUser4 = UserManager.Create(Engineer4, userPWD6);

                //Add IT-Engineer  Role to  Manikandan

                if (chkUser3.Succeeded)
                {
                    IdentityResult result6 = UserManager.AddToRole(Engineer4.Id, "IT-Engineer");

                }

            }
        }
    }
}