﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IT_Asset_Custody.Models
{
    public class Device
    {
        public int ID { get; set; }           //auto generated.
        public string SerialNo { get; set; }   // set inbuilt or not avaliable for spl cases.
        public string ModelNo { get; set; }    
        public string  Make { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime PurchaseDate { get; set; }
        public int PONo { get; set; }
        public int InvoiceNo { get; set; }
        public float Price { get; set; }
        public float CurrentDepriciatedPrice { get; set; }
        public int CurrentlyAssignedTo { get; set; } // set 0 for not assigned to anyone. or set employ id of current assignemt  or set 9999 for scrapp. 
        public string VendorName { get; set; }
        public float  DepriciationPrice(float percent)
        {
            int pYear = PurchaseDate.Year;
            int cYear = DateTime.Now.Year;
            int age = pYear - cYear;
            float dep = 8.5F*age;
            return dep;
        }
    }
    public class PCDevice : Device
    {
        public string MAC { get; set; }
        public string IP { get; set; }
        public string OS { get; set; }
        public string Processor { get; set; }
        public int Ram { get; set; }
        public int Storage { get; set; }
        public string OSName { get; set; } // Should be in the format OSname/type/architecture eg.Windows7/Pro/64bit
        public string OSProductKey { get; set; } // set zero for not activated .
        public bool OEM { get; set; }
        public int MSOfficeVersion { get; set; } // set zero for no office installation.
        public string AntivirusPackage { get; set; }
       // public DateTime MSofficeActivationDate { get; set; }
        //public int MSofficeValidityDays { get; set; }
        public DateTime AvActivationDate { get; set; }
        public int AvValidityDays { get; set; }


    }
    public class PC : PCDevice
    {
        public int CabinetID { get; set; }
        public int UPSID { get; set; }
        public int PrinterID { get; set; }
        public int KeyBoardID { get; set; }
        public int MonitorID { get; set; }
        public int MouseID { get; set; }
        public int SpeakerID { get; set; }
        public string BoardName { get; set; }
        



    }
    public class Laptop : PCDevice
    {
        public int AdaptorID { get; set; }
        public int BattaryID { get; set; }
        public bool Carrycase { get; set; }
    }
    public class DisplayDevice : Device
    {
        public string Type { get; set; }
        public int Size { get; set; }
    }
    public class Monitor : DisplayDevice
    {

    }
    public class TV : DisplayDevice
    {
        
    }
    public class MobileDevice : Device
    {
        public string IMEI1 { get; set; }
        public string IMEI2 { get; set; }                   // set zero for single sim device.
        public string SIM { get; set; }                    // set zero if sim not being issued.
        public string ExpandableStorageSize { get; set; } // set zero for no memory card.
        public string MAC { get; set; }
        public bool Case { get; set; }
        public bool TemperGlass { get; set; }
        public bool Charger { get; set; }
        //public int RAM { get; set; }
        //public string ProcessorType { get; set; }
        
    }
    public class Camera : Device
    {

        public string ExpandableStorageSize { get; set; }   // set zero for no memory card.
        public bool Case { get; set; }
        public decimal Resolution { get; set; }
    }
    public class Mobile : MobileDevice { }
    public class Tablet : MobileDevice { }
    public class Projector : Device { }
    public class DVDPlayer : Device { }
    public class InfraDevice : Device
    {
        public string MAC { get; set; }
        public string IP { get; set; }
        public bool Wireless { get; set; }
        public int NoOfPorts { get; set; }
    }
    public class CCTVCamera : InfraDevice
    {
       public string Resolution { get; set; }
       public string DVRStorage { get; set; }
    }
    public class NetworkSwitch : InfraDevice
    {

    }
    public class Speaker : Device
    {
        public string Type { get; set; }

    }
    public class StorageDevice : Device
    {
       public string Capacity { get; set; }

    }
    public class Harddisk : StorageDevice { }
    public class UPS : Device
    {
        public string BatteryCapacity { get; set; }

    }
    public class PortableDVDWriter : Device
    {
        public string Speed { get; set; }

    }
    public class Printer : Device {
        public string Type { get; set; }

    }
    public class Keyboard : Device { }
    public class Mouse : Device { }
    public class Pendrive : StorageDevice { }
    public class ExternalHDD: StorageDevice { }
    public class FireWall : InfraDevice {
        public DateTime ActivationDate { get; set; }
        public  int ValidityDays { get; set; }
    }






   
}
