﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IT_Asset_Custody.Models
{
    public class Assignment
    {
        public int ID { get; set; }
        public int EmpID { get; set; }
        public int ITEmpID { get; set; }
        public int DevID { get; set; }
        public DateTime AssignDate { get; set; } 
        public DateTime ReleaseDate { get; set; } // stays empty untill relased. and also update device status in device table using a stored procedure.
        public Usage Usage { get; set; }
        public int DocumentPageNum { get; set; }
    }

    public enum Usage
    {
        Office, Programme
    }
}