﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace IT_Asset_Custody.Models
{
    public class Employee
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }             //emp_code.

        public string Name { get; set; }
        public string Email { get; set; }
        public int PillarId { get; set; }
        public long Mobile { get; set; }
        public int BranchID { get; set; }
        public int DesignationID { get; set; }
        public int StateID { get; set; }
        public int DistrictID { get; set; }
        public int CountryID { get; set; }
        public virtual ICollection<Assignment> Assignments { get; set; } 

    }


    

  
}