﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IT_Asset_Custody.Models
{
    public class State
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}