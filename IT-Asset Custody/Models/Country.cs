﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IT_Asset_Custody.Models
{
    public class Country
    {
        public Country()
        {
            Name = "India";
            ID = 1;
        }

        public string Name { get; set; }

        public int ID { get; set; }
       
    }
}