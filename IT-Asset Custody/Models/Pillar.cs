﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IT_Asset_Custody.Models
{
    public class Pillar
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int OrganisationID { get; set; }
    }
}