﻿using System.Web;
using System.Web.Mvc;

namespace IT_Asset_Custody
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
